#!/bin/sh
# -----------------------------------------------------------------------------
# Script to find a keyword in the files in 'data' folder (relative to the script, recursive).
# Matching is whole-world and case-sensitive.
# The found lines will be printed to 'output.txt' (relative to the script).
#
# Usage: ./findKeyword.sh <keyword>
# Example: ./findKeyword.sh apple
# -----------------------------------------------------------------------------

if [ -z "$1" ]; then
  # First parameter does not exist
  echo "No argument provided"
elif [ ! -z "$2" ]; then
  # Second parameter exists
  echo "More than one argument provided"
else
  # Exactly one parameter exists
  keyword=$1

  # grep parameters in use:
  #   -r means: find it recursive
  #   -w means: match only whole words
  #   --no-ignore-case means: match case-sensitive (default)
  #   -h means: suppress the file name prefix on output
  #   after that the directory comes where the search starts
  #   -e is the start of the (regexp) pattern
  #   after that tne pattern comes (by using pre-set keyword)
  # and the output is piped (forwarded) to 'output.txt' file
  grep -rw --no-ignore-case -h "./data" -e "$keyword" > ./output.txt
fi
