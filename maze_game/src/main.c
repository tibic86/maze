#include <stdio.h>
#include <stdlib.h>

#include "terminal.h"
#include "map.h"
#include "draw.h"
#include "game/game.h"

/* Function to print the usage of the executable.
 * Parameter(s):
 *     applicationPath - The path to the executable (first argument of the main function).
 *     message (optional) - The message to be printed before the usage information.
 */
void printUsage(const char *applicationPath, const char *message)
{
    if (message)
    {
        printf("%s\n", message);
    }
    printf("Usage: %s <map file>\n", applicationPath);
    printf("   <map file>    Path to the map descriptor file.\n");
    printf("Examples:\n");
    printf("    %s map.txt\n", applicationPath);
    printf("    %s ../maps/map1.txt\n", applicationPath);
    printf("    %s ~/maze/maps/map1.txt\n", applicationPath);
    printf("    %s /usr/shared/maze/maps/map1.txt\n", applicationPath);
}

/* Function to read user input, trigger game to change the player position.
 * Parameter(s):
 *     game - Contains the player, goal and the game area. Player in it will be changed depending of the user input.
 * Returns with the new state of the game.
 */
enum GameResult stepGame(struct Game *game)
{
    enum GameResult ret;

    /* Get user input */
    char ch;
    disableBuffer();
    scanf(" %c", &ch);
    enableBuffer();

    /* Convert it to movement or other user action */
    switch (ch)
    {
    case 'w': stepPlayer(game, DIR_UP);    break;
    case 'a': stepPlayer(game, DIR_LEFT);  break;
    case 's': stepPlayer(game, DIR_DOWN);  break;
    case 'd': stepPlayer(game, DIR_RIGHT); break;
    case 'u': undoPlayerStep(game, true); break;
    default: /* nothing to do */ break;
    }

    /* Get the new state of the game */
    ret = getGameState(*game);
    return ret;
}

/* Function to evaluate the game.
 * Parameter(s):
 *     applicationPath - path to the executable (used to print the usage info).
 *     mapFilePath - path to the map file.
 *     game - game descriptor structure to be updated
 * Return true, if the initialization succeed.
 * Warning: game shall be destroyed by calling deleteGame in case of initialization succeed.
 */
bool initGame(const char *applicationPath, const char *mapFilePath, struct Game *game)
{
    bool initialized = false;

    /* Try to load the map */
    struct Map map = loadMap(mapFilePath);
    if (!isMapValid(map))
    {
        /* Unable to load the map */
        printUsage(applicationPath, "Unable to load map.");
    }
    else
    {
        /* Create the game */
        *game = createGame(map);
        deleteMap(&map);

        printf("Game created\n");

        if (!isGameValid(*game))
        {
            /* Unable to create the game */
            deleteGame(game);
            printUsage(applicationPath, "Invalid map content.");
        }
        else
        {
            initialized = true;
        }
    }

    return initialized;
}

/* Function to evaluate the game.
 * Parameter(s):
 *     applicationPath - path to the executable (used to print the usage info).
 *     mapFilePath - path to the map file.
 * Returns true, if user won.
 */
bool evalGame(struct Game *game)
{
    bool ret = false;

    /* Make steps until game over */
    enum GameResult result;
    do {
        draw(*game);
        result = stepGame(game);
    } while (result == GAME_RESULT_PENDING);
    draw(*game);

    if (result == GAME_RESULT_WON)
    {
        ret = true;
    }

    return ret;
}

/* The main function what creates, evaluates and destroys the game. */
int main(int argc, char* argv[])
{
    const char *applicationPath = argv[0];

    /* Parse command line parameter(s) */
    if (argc != 2)
    {
        /* Invalid count of command line arguments */
        printUsage(applicationPath, "Invalid command line parameters.");
    }
    else
    {
        /* Try to create the game with the specified map file */
        struct Game game;
        if (initGame(applicationPath, argv[1], &game))
        {
            const bool gameWon = evalGame(&game);
            deleteGame(&game);

            /* Check game result */
            printf("Game over");
            if (gameWon)
            {
                printf(", you won!\n");
            }
            else
            {
                printf(", you lost!\n");
            }
        }
    }

    return 0;
}
