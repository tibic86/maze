#include "linked_list.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/* -------------------------------------------------------------------------- */
/* PRIVATE DECLARATION(S) */
/* -------------------------------------------------------------------------- */

/* Type to represent an item in the linked list */
struct LinkedListItem
{
    void *data;
    struct LinkedListItem *next;
    struct LinkedListItem *prev;
};

/* Function to create a linked list item.
 * Parameter(s):
 *     data (optional) - pointer to the data of the item (it will be copied).
 *     size - the size of the data.
 * Returs with the pointer to the linked list item.
 * Warning: The returned pointer must be destroyed by calling free.
 */
struct LinkedListItem* _createLinkedListItem(const void* data, const unsigned size);


/* -------------------------------------------------------------------------- */
/* PUBLIC FUNCTION DEFINITION(S) */
/* -------------------------------------------------------------------------- */

struct LinkedListHead createLinkedList()
{
    struct LinkedListHead list;

    list.first = NULL;
    list.last = NULL;

    return list;
}

bool isLinkedListValid(const struct LinkedListHead list)
{
    bool valid = true;

    if (!list.first && !list.last)
    {
        /* list is empty */
        valid = true;
    }
    else if (list.first && list.last)
    {
        struct LinkedListItem *item = list.first;

        /* check the chain links */
        bool linksAreCorrect = true;
        while (item->next && linksAreCorrect)
        {
            if (item != item->next->prev)
            {
                linksAreCorrect = false;
            }
            else
            {
                item = item->next;
            }
        }

        if (linksAreCorrect && item == list.last)
        {
            valid = true;
        }
    }

    return valid;
}

void deleteLinkedList(struct LinkedListHead *list)
{
    if (list)
    {
        /* Delete every list item started from the last one */
        struct LinkedListItem *item = list->last;
        while (item)
        {
            struct LinkedListItem *itemToDestroy = item;

            item = itemToDestroy->prev; /* step one back */

            free(itemToDestroy->data); /* destroy item's data */
            free(itemToDestroy); /* destroy the item */
        }
    }
}

bool pushToLinkedList(struct LinkedListHead *list, const void *data, const unsigned size)
{
    bool ret = true;

    if (!list)
    {
        ret = false;
    }
    else
    {
        struct LinkedListItem *newItem = _createLinkedListItem(data, size);

        if (!newItem)
        {
            ret = false;
        }
        else
        {
            struct LinkedListItem *lastItem = list->last;

            newItem->prev = lastItem;
            if (!lastItem)
            {
                /* This is the first item in the list (there was not last item before). */
                list->first = newItem;
            }
            else
            {
                lastItem->next = newItem;
            }
            list->last = newItem;
        }
    }

    return ret;
}

void* popFromLinkedList(struct LinkedListHead *list)
{
    void *ret = NULL;

    if (list && list->last)
    {
        /* List exists and has item in it. */
        struct LinkedListItem *lastItem = list->last;
        struct LinkedListItem *prevItem = lastItem->prev;

        list->last = prevItem;
        if (!prevItem)
        {
            /* There is no more item remain in the list. */
            list->first = NULL;
        }
        else
        {
            /* There is/are another item(s) remain in the list */
            prevItem->next = NULL;
        }

        ret = lastItem->data;
        free(lastItem);
    }

    return ret;
}

void* getLastLinkedListItemData(const struct LinkedListHead list)
{
    void *ret = NULL;

    if (list.last)
    {
        /* List exists and has item in it. */
        ret = list.last->data;
    }

    return ret;
}


/* -------------------------------------------------------------------------- */
/* PRIVATE FUNCTION DEFINITION(S) */
/* -------------------------------------------------------------------------- */

struct LinkedListItem* _createLinkedListItem(const void* data, const unsigned size)
{
    struct LinkedListItem* newItem = (struct LinkedListItem*)malloc(sizeof(struct LinkedListItem));

    if (newItem)
    {
        newItem->next = NULL;
        newItem->prev = NULL;

        if (!data || size <= 0)
        {
            /* Empty item */
            newItem->data = NULL;
        }
        else
        {
            /* Try to copy the item's data */
            newItem->data = malloc(size);
            if (!newItem->data)
            {
                /* Not enough memory */
                free(newItem);
                newItem = NULL;
            }
            else
            {
                /* Copy item's data */
                memcpy(newItem->data, data, size);
            }
        }
    }

    return newItem;
}
