/**
 * This file defines all of the used standard and custom types.
 * There are different simple util functions releated to the custom types also.
 */

#ifndef TYPES_H
#define TYPES_H

#include <stdbool.h>

#ifndef NULL
#define NULL ((void *)0)
#endif

/* Type to represent 2D size */
struct Size
{
    int height;
    int width;
};

/* Type to represent 2D position */
struct Pos {
    int y;
    int x;
};

/* Type to represent direction */
enum Dir {
    DIR_UP,
    DIR_LEFT,
    DIR_DOWN,
    DIR_RIGHT
};

/* Enumeration for the position type of a point relative to a rectangle. */
enum PosInRectResult
{
    POS_OUTSIDE,  /* position is outside of the rectangle and the border of it */
    POS_CORNER,   /* position is on the corner of the border of the rectangle */
    POS_H_BORDER, /* position is on a horizontal border of the rectangle */
    POS_V_BORDER, /* position is on a vertical border of the rectangle */
    POS_INSIDE    /* position is inside the rectangle */
};

/* Function to check two positions are equal or not.
 * Parameter(s):
 *     a - one of the positions
 *     b - other position
 * Returns true, if all dimensions are equal of the positions.
 */
bool eqPos(const struct Pos a, const struct Pos b);

/* Function to calculate the sum of two positions.
 * Parameter(s):
 *     pos - the positions
 *     offset - the offset to be added to the pos
 * Returns the moved position by the offset.
 */
struct Pos movePos(const struct Pos pos, const struct Pos offset);

/* Function to check only the point is inside a rectangle or not
 * Parameter(s):
 *     pos - the point
 *     size - the size of the rectangle (left-top point of the rectangle is always 0,0)
 * Returs true if the point is inside the rectangle (both cordinates are from 0 to the size-1).
 */
bool isPosInside(const struct Pos pos, const struct Size size);

/* Function to calculate the position of a point relative to a "rectangle".
 * Parameter(s):
 *     pos - the point
 *     size - the size of the rectangle (left-top point of the rectangle is always 0,0)
 * Returs with the relative position of the point.
 */
enum PosInRectResult intersectPosAndSize(const struct Pos pos, const struct Size size);

#endif /* TYPES_H */
