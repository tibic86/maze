/**
 * This file is the implementation of the types.h.
 * See it for more details.
 */

#include "types.h"
#include <stdio.h>

bool eqPos(const struct Pos a, const struct Pos b)
{
    return a.y == b.y && a.x == b.x;
}

struct Pos movePos(const struct Pos pos, const struct Pos offset)
{
    struct Pos sum = pos;

    sum.y += offset.y;
    sum.x += offset.x;

    return sum;
}

bool isPosInside(const struct Pos pos, const struct Size size)
{
    bool ret = true;
    
    if (pos.y < 0 || pos.y >= size.height) ret = false; /* y is not inside the rectangle */
    if (pos.x < 0 || pos.x >= size.width) ret = false; /* x is not inside the rectangle */

    return ret;
}

enum PosInRectResult intersectPosAndSize(const struct Pos pos, const struct Size size)
{
    enum PosInRectResult ret;

    if (pos.y < -1 || pos.y > size.height) ret = POS_OUTSIDE; /* y is out of the rectangle and the border of it */
    else if (pos.x < -1 || pos.x > size.width) ret = POS_OUTSIDE; /* x is out of the rectangle and the border of it */
    else {
        const bool hBorder = (pos.y == -1 || pos.y == size.height); /* true, if the y is on one of the horizontal borders */
        const bool vBorder = (pos.x == -1 || pos.x == size.width); /* true, if the x is on one of the vertical borders */
        if (hBorder && vBorder) ret = POS_CORNER; /* position is on the both border, so it is on one of the corners */
        else if (hBorder) ret = POS_H_BORDER; /* position is only on one of the horizontal borders */
        else if (vBorder) ret = POS_V_BORDER; /* position is only on one of the vertical borders */
        else ret = POS_INSIDE; /* position is not outside and not on any of the borders, so it must be inside */
    }

    return ret;
}
