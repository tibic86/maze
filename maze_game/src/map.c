#include "map.h"

#include <stdio.h>
#include <stdlib.h>

#include "utils/types.h"


/* -------------------------------------------------------------------------- */
/* PRIVATE DECLARATION(S) */
/* -------------------------------------------------------------------------- */

/* Function to read the whole content of the map file.
 * Parameter(s):
 *     map - the map descriptor structure to be update.
 *     mapFile - the opened file handler.
 * Note: the map will be updated only if the file contains valid content.
 */
void _readMapFile(struct Map *map, FILE *mapFile);

/* Function to read the item content of the map file (without metadata).
 * Parameter(s):
 *     mapFile - the opened file handler at the position of the start of the items.
 *     mapSize - the size of the map (have to be pre-read from the metadata).
 *     itemCount - the count of the map item (have to be pre-read from the metadata).
 * Returns the array of the map items, if the content is not valid, NULL will be returned.
 */
struct MapItem* _readMapFileContent(FILE *mapFile, const struct Size mapSize, const int itemCount);

/* Function to check an item data and update the map item if it is valid.
 * Parameter(s):
 *     mapItems - the list of the items.
 *     mapSize - the size of the map.
 *     itemIndex - the index of the new item.
 *     pos - the position of the input item.
 *     type - the type data of the input item.
 * Returns true, if the input item data is valid and mapItem is updated.
 */
bool _setMapItem(struct MapItem *mapItems, const struct Size mapSize, const int itemIndex, const struct Pos pos, const int type);


/* -------------------------------------------------------------------------- */
/* PUBLIC FUNCTION DEFINITION(S) */
/* -------------------------------------------------------------------------- */

struct Map loadMap(const char *filePath)
{
    /* Initialize map to an empty one */
    struct Map map;
    map.size.height = 0;
    map.size.width = 0;
    map.itemCount = 0;
    map.items = NULL;

    if (filePath)
    {
        /* Open the specified file */
        FILE *mapFile = fopen(filePath, "rt");
        if (mapFile)
        {
            /* Read the file than close it */
            _readMapFile(&map, mapFile);
            fclose(mapFile);
        }
    }

    return map;
}

bool isMapValid(const struct Map map)
{
    bool valid = true;

    if (map.size.height <= 0 || map.size.width <= 0)
    {
        valid = false;
    }
    else if (map.itemCount < 0)
    {
        valid = false;
    }
    else if (map.itemCount > 0)
    {
        if (!map.items)
        {
            valid = false;
        }
    }

    return valid;
}

void deleteMap(struct Map *map)
{
    free(map->items);

    map->size.height = 0;
    map->size.width = 0;
    map->itemCount = 0;
    map->items = NULL;
}

/* -------------------------------------------------------------------------- */
/* PRIVATE FUNCTION DEFINITION(S) */
/* -------------------------------------------------------------------------- */

void _readMapFile(struct Map *map, FILE *mapFile)
{
    /* Read file header */
    int itemCount;
    struct Size mapSize;
    int readCount = fscanf(mapFile, "%d %d %d\n", &itemCount, &mapSize.height, &mapSize.width);
    if (readCount == 3 && itemCount >= 0 && mapSize.height > 2 && mapSize.width > 2)
    {
        /* Metadata had read from the file */
        mapSize.height -= 2; /* size in the file contains the border of the area too */
        mapSize.width -= 2;

        /* Read map content */
        map->items = _readMapFileContent(mapFile, mapSize, itemCount);
        if (map->items)
        {
            /* Content reading succeed, copy metadata to the map descriptor structure */
            map->size = mapSize;
            map->itemCount = itemCount;
        }
    }
}

struct MapItem* _readMapFileContent(FILE *mapFile, const struct Size mapSize, const int itemCount)
{
    struct MapItem *items = (struct MapItem*)malloc(itemCount * sizeof(struct MapItem));
    if (items)
    {
        /* Read all map items from the file */
        bool valid = true;
        int itemIndex = 0;
        while (!feof(mapFile) && valid)
        {
            /* File is not at the end and no error found yet */
            if (itemIndex >= itemCount)
            {
                /* File contains more item(s) than the metadata describes */
                valid = false;
            }
            else
            {
                /* Read next item from the file */
                struct Pos pos;
                int type;
                int readCount = fscanf(mapFile, "%d %d %d\n", &pos.y, &pos.x, &type);
                if (readCount != 3)
                {
                    /* Invalid line read */
                    valid = false;
                }
                else
                {
                    pos.x -= 1; /* first row and column is for the border, remove this offset */
                    pos.y -= 1;

                    /* Set item in the list */
                    valid &= _setMapItem(items, mapSize, itemIndex, pos, type);
                }
                itemIndex++;
            }
        }

        if (!valid)
        {
            /* Error found in the content */
            free(items);
            items = NULL;
        }
    }

    return items;
}

bool _setMapItem(struct MapItem *mapItems, const struct Size mapSize, const int itemIndex, const struct Pos pos, const int type)
{
    bool valid = true;

    if (!isPosInside(pos, mapSize))
    {
        /* Position is out of the map */
        valid = false;
    }
    else
    {
        enum MapItemType itemType;
        int i;

        /* Convert the type of the item */
        switch (type)
        {
        case 0: itemType = MAP_ITEM_PLAYER; break;
        case 1: itemType = MAP_ITEM_SNAKE;  break;
        case 2: itemType = MAP_ITEM_GOAL;   break;
        case 3: itemType = MAP_ITEM_WALL;   break;
        default: valid = false; break;
        }

        /* Check the positions of previous items */
        for(i = 0; valid && i < itemIndex; ++i)
        {
            if (eqPos(mapItems[i].pos, pos))
            {
                valid = false;
            }
        }

        if (valid)
        {
            /* Type is valid, update map item */
            mapItems[itemIndex].type = itemType;
            mapItems[itemIndex].pos = pos;
        }
    }

    return valid;
}
