/**
 * This file defines a structure to store every information about the game map and items in it.
 * There is a function to load it from a file, to validate and delete it after usage.
 */

#ifndef MAP_H
#define MAP_H

#include "utils/types.h"

/* Enumeration type of the map item types */
enum MapItemType
{
    MAP_ITEM_SNAKE,
    MAP_ITEM_PLAYER,
    MAP_ITEM_GOAL,
    MAP_ITEM_WALL
};

/* Type to represent a map item */
struct MapItem
{
    enum MapItemType type;
    struct Pos pos;
};

/* Type to represent a map */
struct Map
{
    struct Size size;
    int itemCount;
    struct MapItem *items;
};

/* Function to load the map from file.
 * Parameter(s):
 *     filePath - path to the map descriptor file
 * Returns with the loaded map.
 * Warning: The map must be destroyed by calling deleteMap function
 * Note: if the load was not successful, size and itemCount will be zero and items will be NULL.
 */
struct Map loadMap(const char *filePath);

/* Function to check the map is valid.
 * Parameter(s):
 *     map - the map to be checked.
 * Returns true if the map is valid and the size is not zero.
 */
bool isMapValid(const struct Map map);

/* Function to delete a map.
 * Parameter(s):
 *     map - the map to be destroyed.
 */
void deleteMap(struct Map *map);

#endif /* MAP_H */
