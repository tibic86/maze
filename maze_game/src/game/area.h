/**
 * This file defines a structure to store every information releated to the drawable game area.
 * There is a function to allocate and initialize it and there is another one to deallocate it.
 */

#ifndef AREA_H
#define AREA_H

#include "utils/types.h"

/* Structure to represent a 2D area with filled/free state on every field */
struct Area {
    struct Size size;
    bool** fields;
};

/* Function to create an empty area (area fields will be not set).
 * Parameter(s):
 *     size - the size of the new area.
 * Returns with the created area descriptor structure.
 * Warning: The returned area must be deinitialized by calling deleteArea function!
 */
struct Area createArea(const struct Size size);

/* Function to create an area based on another (fields will be copied from it).
 * Parameter(s):
 *     area - the area what shall be copied.
 * Retruns with the copied area destructor structure.
 * Warning: The returned area must be deinitialized by calling deleteArea function!
 */
struct Area copyArea(const struct Area area);

/* Function to check the area is valid.
 * Parameter(s):
 *     area - the area to be checked.
 * Returns true if the area is valid and the size is not zero...
 */
bool isAreaValid(const struct Area area);

/* Function to deinitialize (free up) an area.
 * Parameter(s):
 *     area - the area to be destroyed.
 */
void deleteArea(struct Area *area);

/* Function to set the value of a field in an area.
 * Parameter(s):
 *     area - the area to be modified.
 *     pos - the position of the field (inside the area).
 *     value - the desired value of the field.
 * Returns true, if the field value is changed.
 * Note: if the position is out of the area, nothing will be changed.
 */
bool setAreaField(struct Area *area, const struct Pos pos, const bool value);

/* Function to get the actual value of a field in an area.
 * Parameter(s):
 *     area - the area to be modified.
 *     pos - the position of the field (inside the area).
 *     defaultValue - the value to be returned if the position is out of the area.
 * Returns with the value of the field.
 * Note: if the position is out of the area, defaultValue will be returned.
 */
bool getAreaField(const struct Area area, const struct Pos pos, const bool defaultValue);

#endif /* AREA_H */
