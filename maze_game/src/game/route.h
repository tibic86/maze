/**
 * This file defines a structure to store a history of a game unit movement.
 * There is a function to allocate and initialize it and there is anothers to add, remove a step.
 */

#ifndef ROUTE_H
#define ROUTE_H

#include "utils/types.h"
#include "utils/linked_list.h"

/* Type to represent a route */
struct Route
{
    struct LinkedListHead steps;
    struct Pos endPos;
};

/* Function to create an empty route
 * Returns with the route.
 * Warning: the route must be destroyed by calling deleteRoute function!
 */
struct Route createRoute();

/* Function to check the route is valid.
 * Parameter(s):
 *     route - the route to be checked.
 * Returns true if the route is valid.
 */
bool isRouteValid(const struct Route route);

/* Function to delete a route.
 * Parameter(s):
 *     route - the route to be deleted.
 */
void deleteRoute(struct Route *route);

/* Function to add a step to a route.
 * Parameter(s):
 *     route - the route to be updated.
 *     dir - the direction of the step.
 *     positionValidator (optional) - function to validate the new position.
 *     funcData (optional) - pointer to a data what will be forwarded to the positionValidator function.
 * Returns true, if the step is possible.
 */
bool addStepToRoute(struct Route *route, const enum Dir dir, bool (*positionValidator)(const struct Pos, void*), void *funcData);

/* Function to remove the last step from a route.
 * Parameter(s):
 *     route - the route to be updated.
 * Returns true, if there is a step successfully removed.
 */
bool removeLastStepFromRoute(struct Route *route);

/* Function to get the last step direction.
 * Parameter(s):
 *     route - the route to be checked.
 *     defaultDir - the returned direction if there is not any step recorded on the route.
 * Returns with the last step direction or with the defaultDir if there was not any step.
 */
enum Dir getRouteEndDir(const struct Route route, const enum Dir defaultDir);

#endif /* ROUTE_H */
