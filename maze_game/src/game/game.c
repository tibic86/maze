/**
 * This file is the implementation of the game.h.
 * See it for more details.
 */

#include "game.h"

#include <stdlib.h>
#include <stdio.h>

#include "snake.h"

/* -------------------------------------------------------------------------- */
/* PRIVATE FUNCTION DECLARATION(S) */
/* -------------------------------------------------------------------------- */

/* Function to validate a position for the player unit.
 * Parameter(s):
 *     pos - the position to be checked.
 *     gameData - the pointer to the game.
 * Returns true, if the player can step to the position.
 */
bool _checkPlayerPosition(const struct Pos pos, void *gameData);


/* -------------------------------------------------------------------------- */
/* PUBLIC FUNCTION DEFINITION(S) */
/* -------------------------------------------------------------------------- */
struct Game createGame(const struct Map map)
{
    const struct Pos zeroPos = { 0, 0 };
    struct Game game;

    bool multipleWalls = false;
    int snakeCounter = 0;
    int playerCounter = 0;
    int goalCounter = 0;
    int itemIdx;

    /* Pre-initialization */
    game.area = createArea(map.size);
    game.playerRoute = createRoute();
    game.snakePos = zeroPos;
    game.snakePosHistory = createLinkedList();
    game.goalPos = zeroPos;

    /* Set game units by the map items */
    for(itemIdx = 0; itemIdx < map.itemCount; ++itemIdx)
    {
        const struct MapItem item = map.items[itemIdx];

        switch (item.type)
        {
        case MAP_ITEM_PLAYER:
            playerCounter++;
            game.playerRoute.endPos = item.pos;
            break;

        case MAP_ITEM_SNAKE:
            snakeCounter++;
            game.snakePos = item.pos;
            break;

        case MAP_ITEM_GOAL:
            goalCounter++;
            game.goalPos = item.pos;
            break;

        case MAP_ITEM_WALL:
            if (!setAreaField(&game.area, item.pos, true))
            {
                multipleWalls = true;
            }
            break;
        }
    }

    if (multipleWalls || playerCounter != 1 || snakeCounter != 1 || goalCounter != 1)
    {
        /* These types of maps are not handled actually */
        deleteGame(&game);
    }

    return game;
}

bool isGameValid(const struct Game game)
{
    bool valid = true;

    if (!isAreaValid(game.area))
    {
        /* Area is not valid */
        valid = false;
    }
    else if (!isRouteValid(game.playerRoute))
    {
        /* Route is not valid */
        valid = false;
    }
    else if (!isLinkedListValid(game.snakePosHistory))
    {
        /* Snake pos history is not valid */
        valid = false;
    }

    return valid;
}

void deleteGame(struct Game *game)
{
    if (game)
    {
        const struct Pos zeroPos = { 0, 0 };

        deleteArea(&game->area);
        deleteRoute(&game->playerRoute);
        game->snakePos = zeroPos;
        deleteLinkedList(&game->snakePosHistory);
        game->goalPos = zeroPos;
    }
}

void stepPlayer(struct Game *game, const enum Dir direction)
{
    /* Add player position and direction to the history */
    addStepToRoute(&game->playerRoute, direction, &_checkPlayerPosition, game);

    /* Add snake position to the history */
    pushToLinkedList(&game->snakePosHistory, &game->snakePos, sizeof(game->snakePos));
    game->snakePos = calcSnakeStep(game->area, game->goalPos, game->playerRoute.endPos, game->snakePos);
}

void undoPlayerStep(struct Game *game, const bool stepSnake)
{
    void *snakePosData;

    /* Update player position from history */
    removeLastStepFromRoute(&game->playerRoute);

    /* Update snake position from history */
    snakePosData = popFromLinkedList(&game->snakePosHistory);
    if (snakePosData)
    {
        struct Pos *prevSnakePos = (struct Pos*)snakePosData;
        game->snakePos = *prevSnakePos;
    }
}

enum Dir getPlayerDir(const struct Game game)
{
    const enum Dir dir = getRouteEndDir(game.playerRoute, DIR_UP);
    return dir;
}

enum GameResult getGameState(const struct Game game)
{
    enum GameResult ret;

    const bool snakeAtePlayer = eqPos(game.playerRoute.endPos, game.snakePos);
    const bool goalReached = eqPos(game.playerRoute.endPos, game.goalPos);
    if (snakeAtePlayer)
    {
        ret = GAME_RESULT_LOST;
    }
    else if (goalReached)
    {
        ret = GAME_RESULT_WON;
    }
    else
    {
        ret = GAME_RESULT_PENDING;
    }

    return ret;
}


/* -------------------------------------------------------------------------- */
/* PRIVATE FUNCTION DEFINITION(S) */
/* -------------------------------------------------------------------------- */

bool _checkPlayerPosition(const struct Pos pos, void *gameData)
{
    bool ret = true;
    struct Game *game = (struct Game*)gameData;

    if (getAreaField(game->area, pos, true))
    {
        /* There is a wall on the position or it is out of the game area */
        ret = false;
    }

    return ret;
}
