/**
 * This file contains custom types and functions to handle the game state.
 * Game state consists of the Player state, goal position and the drawable game area.
 * Player state consists of the position and the actual direction of the player.
 */

#ifndef GAME_H
#define GAME_H

#include "map.h"
#include "area.h"
#include "utils/types.h"
#include "route.h"

/* Type to represent the state of the game */
struct Game {
    struct Area area;
    struct Route playerRoute;
    struct Pos snakePos;
    struct LinkedListHead snakePosHistory;
    struct Pos goalPos;
};

/* Enumeration type to return the game state */
enum GameResult {
    GAME_RESULT_PENDING,
    GAME_RESULT_WON,
    GAME_RESULT_LOST
};

/* Function to create an "empty" game.
 * Parameter(s):
 *     map - the initial state descriptor map structure.
 * Returns with the created game descriptor structure.
 * Warning: The game must be deinitialized by calling deleteGame function!
 * Note: initialization shall be done manually or by a game descriptor file.
 */
struct Game createGame(const struct Map map);

/* Function to check the game is valid.
 * Parameter(s):
 *     game - the game to be checked.
 * Returns true if the game is valid and the size is not zero...
 */
bool isGameValid(const struct Game game);

/* Function to deinitialize the game.
 * Parameter(s):
 *     game - the game to be deinitialized.
 * Warning: Game shall not be used after this call!
 */
void deleteGame(struct Game *game);

/* Function to step the player (and the snake as well).
 * Parameter(s):
 *     game - the game what will be updated
 */
void stepPlayer(struct Game *game, const enum Dir direction);

/* Function to step the player back to the previous position (and the snake to the next one).
 * Parameter(s):
 *     game - the game what will be updated
 * Returns with true if the undo was possible.
 */
void undoPlayerStep(struct Game *game, const bool moveSnake);

/* Function to get the current view direction of the player.
 * Parameter(s):
 *     game - the game itself with all properties and items.
 * Returns with the orientation of the player.
 */
enum Dir getPlayerDir(const struct Game game);

/* Function to check the goal is reached by the player, snake eats the player or not.
 * Parameter(s):
 *     game - The game itself with all properties and items.
 * Returns with the state of the game.
 */
enum GameResult getGameState(const struct Game game);

#endif /* GAME_H */
