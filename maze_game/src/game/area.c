/**
 * This file is the implementation of the area.h.
 * See it for more details.
 */

#include "area.h"

#include <stdio.h>
#include <stdlib.h>

/* -------------------------------------------------------------------------- */
/* PRIVATE FUNCTION DECLARATION(S) */
/* -------------------------------------------------------------------------- */

/* Function to create an area with initialization.
 * Parameter(s):
 *     size - the desired size of the area
 *     fieldsToCopy (optional) - if not NULL, the values of the area will be initialized from this 2D array.
 * Returns with the allocated, initialized area.
 * Warning: area shall be destroyed by calling deleteArea function.
 */
struct Area _createArea(const struct Size size, bool **fieldsToCopy);


/* -------------------------------------------------------------------------- */
/* PUBLIC FUNCTION DEFINITION(S) */
/* -------------------------------------------------------------------------- */
struct Area createArea(const struct Size size)
{
    const struct Area ret = _createArea(size, NULL);
    return ret;
}

struct Area copyArea(const struct Area area)
{
    const struct Area ret = _createArea(area.size, area.fields);
    return ret;
}

bool isAreaValid(const struct Area area)
{
    bool valid = true;

    if (area.size.height <= 0 || area.size.width <= 0 || !area.fields)
    {
        valid = false;
    }
    else
    {
        int row;
        for(row = 0; row < area.size.height; ++row)
        {
            if (!area.fields[row])
            {
                valid = false;
            }
        }
    }

    return valid;
}

void deleteArea(struct Area *area)
{
    if (area)
    {
        /* Clear the content */
        int y;
        for (y = 0; y < area->size.height; ++y) {
            free(area->fields[y]); /* clears the row only */
        }
        free(area->fields); /* clears the container of the row pointers */

        area->size.height = 0;
        area->size.width = 0;
        area->fields = NULL;
    }
}

bool setAreaField(struct Area *area, const struct Pos pos, const bool value)
{
    bool isChanged = false;

    if (isPosInside(pos, area->size))
    {
        if (value != area->fields[pos.y][pos.x])
        {
            isChanged = true;
            area->fields[pos.y][pos.x] = value;
        }
    }

    return isChanged;
}

bool getAreaField(const struct Area area, const struct Pos pos, const bool defaultValue)
{
    bool ret = defaultValue;

    if (isPosInside(pos, area.size))
    {
        /* The position is inside the area, get the field value */
        ret = area.fields[pos.y][pos.x];
    }

    return ret;
}


/* -------------------------------------------------------------------------- */
/* PRIVATE FUNCTION DEFINITION(S) */
/* -------------------------------------------------------------------------- */

struct Area _createArea(const struct Size size, bool **fieldsToCopy)
{
    struct Area ret;

    if (size.height <= 0 || size.width <= 0)
    {
        /* Input is invalid or the size is zero */
        ret.size.height = 0;
        ret.size.width = 0;

        ret.fields = NULL;
    }
    else
    {
        int y;

        /* Set other parameters of the area */
        ret.size.height = size.height;
        ret.size.width = size.width;

        /* Allocate a 2D map and initialize the values */
        ret.fields = (bool**)malloc(sizeof(bool*) * (size.height));
        for (y = 0; y < size.height; ++y) {
            int x;
            ret.fields[y] = (bool*)malloc(sizeof(bool) * (size.width));
            for(x = 0; x < size.width; ++x) {
                if (fieldsToCopy)
                {
                    ret.fields[y][x] = fieldsToCopy[y][x]; /* copy the content from the input */
                }
                else
                {
                    ret.fields[y][x] = false; /* in default the field is empty */
                }
            }
        }
    }

    return ret;
}
