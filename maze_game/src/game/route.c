#include "route.h"

#include <stdio.h>
#include <stdlib.h>

/* -------------------------------------------------------------------------- */
/* PRIVATE DECLARATION(S) */
/* -------------------------------------------------------------------------- */

/* Type to represent a step on a route */
struct _RouteStep
{
    struct Pos startPos;
    enum Dir stepDir;
};

/* Function to calculate the end position of a step.
 * Parameter(s):
 *     startPos - the start position of the step
 *     stepDir - the step direction
 * Returns with the end position of the step.
 */
struct Pos _calcStepEndPos(const struct Pos startPos, const enum Dir stepDir);


/* -------------------------------------------------------------------------- */
/* PUBLIC FUNCTION DEFINITION(S) */
/* -------------------------------------------------------------------------- */

struct Route createRoute()
{
    const struct Pos zeroPos = { 0, 0 };
    struct Route ret;

    ret.steps = createLinkedList();
    ret.endPos = zeroPos;

    return ret;
}

bool isRouteValid(const struct Route route)
{
    bool valid = true;

    if (!isLinkedListValid(route.steps))
    {
        valid = false;
    }

    return valid;
}

void deleteRoute(struct Route *route)
{
    if (route)
    {
        const struct Pos zeroPos = { 0, 0 };

        deleteLinkedList(&route->steps);
        route->endPos = zeroPos;
    }
}

enum Dir getRouteEndDir(const struct Route route, const enum Dir defaultDir)
{
    enum Dir dir = defaultDir;
    void *routeStepData = getLastLinkedListItemData(route.steps);

    if (routeStepData)
    {
        /* There is steps in the route */
        struct _RouteStep* routeItem = (struct _RouteStep*)routeStepData;
        dir = routeItem->stepDir;
    }

    return dir;
}

bool addStepToRoute(struct Route *route, const enum Dir dir, bool (*positionValidator)(const struct Pos, void*), void *funcData)
{
    bool ret = false;

    if (route)
    {
        struct _RouteStep step;

        /* Calculate new position */
        const struct Pos newPos = _calcStepEndPos(route->endPos, dir);

        /* Check the new position */
        bool valid = true;
        if (positionValidator)
        {
            valid = (*positionValidator)(newPos, funcData);
        }

        /* Update step history */
        step.startPos = route->endPos;
        step.stepDir = dir;
        pushToLinkedList(&route->steps, &step, sizeof(step));

        if (valid)
        {
            /* Update route end position */
            route->endPos = newPos;

            /* Set return value */
            ret = true;
        }
    }

    return ret;
}

bool removeLastStepFromRoute(struct Route *route)
{
    bool ret = false;

    if (route)
    {
        /* Pop last item from step list */
        struct _RouteStep *lastItem = (struct _RouteStep*)popFromLinkedList(&route->steps);
        if (lastItem)
        {
            /* There was item in the list, update route end position */
            route->endPos = lastItem->startPos;

            /* Delete the removed last item */
            free(lastItem);
        }
    }

    return ret;
}


/* -------------------------------------------------------------------------- */
/* PRIVATE FUNCTION DEFINITION(S) */
/* -------------------------------------------------------------------------- */

struct Pos _calcStepEndPos(const struct Pos startPos, const enum Dir stepDir)
{
    struct Pos endPos = startPos;

    switch (stepDir)
    {
    case DIR_UP:    endPos.y--; break;
    case DIR_LEFT:  endPos.x--; break;
    case DIR_DOWN:  endPos.y++; break;
    case DIR_RIGHT: endPos.x++; break;
    }

    return endPos;
}
