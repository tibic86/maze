#include "snake.h"

#include <stdio.h>

#include "route.h"

/* -------------------------------------------------------------------------- */
/* PRIVATE FUNCTION DECLARATION(S) */
/* -------------------------------------------------------------------------- */

/* Function to set reachable fields by one step from a position.
 * Parameter(s):
 *     area - the game labirinth.
 *     goalPos - position what is unable to reached by the snake.
 *     floodedArea - the reachable positions to be set.
 *     snakePos - the position.
 * Returns true, if any new position is set.
 */
bool _floodSnakePos(const struct Area area, const struct Pos goalPos, struct Area *floodedArea, const struct Pos snakePos);

/* Function to set reachable positions with one more step.
 * Parameter(s):
 *     area - the game labirinth
 *     goalPos - position what is unable to reached by the snake.
 *     floodedArea - the actually reachable positions to be update.
 * Returns true, if any new position is set.
 * Details:
 *     In the floodedArea, the fields are set if the field is reachable by the snake in the previous step(s).
 *     This function sets the field(s) which are reachable by the snake with one more step and
 *     returns true if there is any new field set.
 */
bool _floodSnakes(const struct Area area, const struct Pos goalPos, struct Area *floodedArea);

/* Function to calculate the minimum step count from the start pos to reach the target one.
 * Parameter(s):
 *     area - the game labirinth
 *     goalPos - position what is unable to reached by the snake.
 *     targetPos - the position to be reach
 *     startPos - the start position
 * Returns with the minimum step count to reach the target or with -1 if it is not reachable.
 */
int _calcSnakeMinSteps(const struct Area area, const struct Pos goalPos, const struct Pos targetPos, const struct Pos startPos);


/* -------------------------------------------------------------------------- */
/* PUBLIC FUNCTION DEFINITION(S) */
/* -------------------------------------------------------------------------- */

struct Pos calcSnakeStep(const struct Area area, const struct Pos goalPos, const struct Pos targetPos, const struct Pos startPos)
{
    struct Pos choosenStepPos = startPos;

    if (!eqPos(startPos, targetPos))
    {
        /* Initialize the minimum route length from the actual snake position */
        int bestDist = _calcSnakeMinSteps(area, goalPos, targetPos, startPos);

        if (bestDist > 0)
        {
            /* Player is reachable from the actual position and it requires at least one step.
             * Check all available movement to find a shorter route to the player.
             * (Note: better strategy will be to go near to the goal and 'wait' until player gets close to it...)
             */

            /* Create a constant array with all available steps */
            const struct Pos stepDeltas[] = {
                { -1,  0 }, /* up */
                {  0, -1 }, /* left */
                { +1,  0 }, /* down */
                {  0, +1 }  /* right */
            };

            /* Iterate through all available steps and check the length of the shortest route to the player */
            int i;
            for(i = 0; i < sizeof(stepDeltas)/sizeof(stepDeltas[0]); ++i)
            {
                const struct Pos stepPos = movePos(startPos, stepDeltas[i]);
                const int currentDist = _calcSnakeMinSteps(area, goalPos, targetPos, stepPos);

                if (currentDist >= 0)
                {
                    /* Player can be reached from this position */
                    if (currentDist < bestDist)
                    {
                        /* If the snake steps to the actual position, it has a shorter way to the player.
                         * Update the choosen step and the distance. */
                        bestDist = currentDist;
                        choosenStepPos = stepPos;
                    }
                }
            }
        }
    }

    return choosenStepPos;
}


/* -------------------------------------------------------------------------- */
/* PRIVATE FUNCTION DEFINITION(S) */
/* -------------------------------------------------------------------------- */

bool _floodSnakePos(const struct Area area, const struct Pos goalPos, struct Area *floodedArea, const struct Pos snakePos)
{
    /* create a constant with all available snake steps */
    const struct Pos stepDeltas[] = {
        { -1,  0 }, /* one step up */
        {  0, -1 }, /* one step left */
        { +1,  0 }, /* one step down */
        {  0, +1 }  /* one step right */
    };
    int i;
    bool floodedAny = false;

    /* iterate over all reachable positions by the snake */
    for(i = 0; i < sizeof(stepDeltas) / sizeof(stepDeltas[0]); ++i)
    {
        const struct Pos stepPos = movePos(snakePos, stepDeltas[i]);
        bool posIsFree = true;

        if (getAreaField(area, stepPos, true))
        {
            /* position is a wall of outside of the area */
            posIsFree = false;
        }
        else if (eqPos(goalPos, stepPos))
        {
            /* position is equals with the goal */
            posIsFree = false;
        }

        if (posIsFree)
        {
            /* position can be reached by the snake */
            floodedAny |= setAreaField(floodedArea, stepPos, true);
        }
    }

    return floodedAny;
}

bool _floodSnakes(const struct Area area, const struct Pos goalPos, struct Area *floodedArea)
{
    bool floodedAny = false;

    /* iterate over the full area */
    struct Pos pos;
    struct Area prevFloodedArea = copyArea(*floodedArea);
    for(pos.y = 0; pos.y < prevFloodedArea.size.height; ++pos.y)
    {
        for(pos.x = 0; pos.x < prevFloodedArea.size.width; ++pos.x)
        {
            if (getAreaField(prevFloodedArea, pos, false))
            {
                /* actual position was already reached by the snake, set neighbours */
                floodedAny |= _floodSnakePos(area, goalPos, floodedArea, pos);
            }
        }
    }
    deleteArea(&prevFloodedArea);

    return floodedAny;
}

int _calcSnakeMinSteps(const struct Area area, const struct Pos goalPos, const struct Pos targetPos, const struct Pos startPos)
{
    int steps = 0;

    if (getAreaField(area, startPos, true) || eqPos(startPos, goalPos))
    {
        /* Start position is a wall, outside or equals with the goal */
        /* (start and target position may be equal, but it is ignored here) */
        steps = -1;
    }
    else
    {
        /* Create an area to store the reachable positions by the snake */
        struct Area floodedArea = createArea(area.size);
        setAreaField(&floodedArea, startPos, true); /* sets the start position of the snake */

        /* Update the reachable positions from step to step */
        while (!getAreaField(floodedArea, targetPos, false) && steps >= 0)
        {
            /* Target position is not reached yet */
            if (!_floodSnakes(area, goalPos, &floodedArea))
            {
                /* No more field is reachable, unable to reach the target */
                steps = -1;
            }
            else
            {
                /* Reachable positions updated, increment step counter */
                steps++;
            }
        }
        deleteArea(&floodedArea);
    }

    return steps;
}
