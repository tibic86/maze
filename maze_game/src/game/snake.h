/**
 * This file defines a function to calculate the best next step of the snake.
 */

#ifndef SNAKE_H
#define SNAKE_H

#include "utils/types.h"
#include "area.h"

/* Function to calculate the next snake step.
 * Parameter(s):
 *     area - the game labirinth
 *     goalPos - position what is unable to reached by the snake.
 *     targetPos - the position shall be reached
 *     startPos - the actual position of the snake
 * Returns with the selected new position of the snake.
 */
struct Pos calcSnakeStep(const struct Area area, const struct Pos goalPos, const struct Pos targetPos, const struct Pos startPos);

#endif /* SNAKE_H */
