/**
 * This file contains the drawing releated function.
 * It uses Game state structure to determine the border, player, snake, goal and wall positions.
 */

#ifndef DRAW_H
#define DRAW_H

#include "game/game.h"

/* Function to draw the whole game area (with borders).
 * Parameter(s):
 *     game - The game itself with all properties and items.
 */
void draw(const struct Game game);

#endif /* DRAW_H */
