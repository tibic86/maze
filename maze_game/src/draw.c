/**
 * This file is the implementation of the draw.h.
 * See it for more details.
 */

#include "draw.h"

#include <stdio.h>
#include <stdlib.h>

/* -------------------------------------------------------------------------- */
/* PRIVATE FUNCTION DECLARATIONS */
/* -------------------------------------------------------------------------- */

/* Function to get the character to print for any position.
 * Parameter(s):
 *     pos - The position.
 *     game - The game itself with all properties and items.
 * Returns with the character to print for the game item at the requested point if any, otherwise returns with ' ' (space).
 * Note: The returned character is differs by the state of the item if it has any (e.g. player character depends on the direction of the player).
 */
char _getDrawChar(const struct Pos pos, const struct Game game);

/* Function to get the character to print for the game area (inside).
 * Parameter(s):
 *     pos - The position.
 *     game - The game itself with all properties and items.
 */
char _getAreaDrawChar(const struct Pos pos, const struct Game game);

/* Function to get the character to print for the player position.
 * Parameter(s):
 *     dir - The actual direction of the player.
 */
char _getPlayerDrawChar(const enum Dir dir);

/* -------------------------------------------------------------------------- */
/* PUBLIC FUNCTION DEFINITIONS */
/* -------------------------------------------------------------------------- */

void draw(const struct Game game)
{
    const int width = game.area.size.width + 2; /* +2 because the borders shall be printed too */
    const int lineLen = width + 1; /* +1 because of the closing \0 */
    char *line;
    struct Pos pos;

    /* fill the whole string */
    system("clear");
    line = (char*)malloc(lineLen * sizeof(char));
    for(pos.y = -1; pos.y <= game.area.size.height; ++pos.y)
    {
        /* fill one line */
        int characterIdx = 0;
        for(pos.x = -1; pos.x <= game.area.size.width; ++pos.x)
        {
            line[characterIdx++] = _getDrawChar(pos, game);
        }
        line[characterIdx] = '\0';
        printf("%s\n", line);
    }
    free(line);
}

/* -------------------------------------------------------------------------- */
/* PRIVATE FUNCTION DEFINITIONS */
/* -------------------------------------------------------------------------- */

char _getDrawChar(const struct Pos pos, const struct Game game)
{
    char ret;

    /* check relationship between the point and the game area / rectangle */
    const enum PosInRectResult posType = intersectPosAndSize(pos, game.area.size);
    switch (posType)
    {
    case POS_OUTSIDE:  ret = ' '; break; /* position is outside of the game area and the border */
    case POS_CORNER:   ret = '#'; break; /* position equals with one of a corner of the border one */
    case POS_H_BORDER: ret = '-'; break; /* position is one of a horizontal border one */
    case POS_V_BORDER: ret = '|'; break; /* position is one of a vertical border one */
    case POS_INSIDE:   ret = _getAreaDrawChar(pos, game); break; /* position is inside the game area */
    }
    
    return ret;
}

char _getAreaDrawChar(const struct Pos pos, const struct Game game)
{
    char ret;

    if (eqPos(pos, game.snakePos))
    {
        ret = '~';
    }
    else if (eqPos(pos, game.playerRoute.endPos))
    {
        /* position equals with the player position */
        const enum Dir dir = getPlayerDir(game);
        ret = _getPlayerDrawChar(dir);
    }
    else if (eqPos(pos, game.goalPos))
    {
        /* position equals with the goal position */
        ret = 'x';
    }
    else if (getAreaField(game.area, pos, false))
    {
        /* position is one of a wall one */
        ret = 'o';
    }
    else
    {
        /* position is one of an empty one */
        ret = ' ';
    }

    return ret;
}

char _getPlayerDrawChar(const enum Dir dir)
{
    char ret;

    switch (dir)
    {
    case DIR_UP:    ret = '^'; break;
    case DIR_RIGHT: ret = '>'; break;
    case DIR_DOWN:  ret = 'v'; break;
    case DIR_LEFT:  ret = '<'; break;
    }

    return ret;
}
