#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "terminal.h"
#include "calc/calc.h"

/* Function to print the usage of the executable.
 * Parameter(s):
 *     applicationPath - The path to the executable (first argument of the main function).
 *     message (optional) - The message to be printed before the usage information.
 */
void printUsage(const char *applicationPath, const char *message)
{
    if (message)
    {
        printf("%s\n", message);
    }
    printf("Usage: %s <output file>\n", applicationPath);
    printf("   <output file>    Path to a file to write the calculation history on exit.\n");
    printf("Examples:\n");
    printf("    %s hist.txt\n", applicationPath);
    printf("    %s ../out/calc_hist.txt\n", applicationPath);
    printf("    %s ~/calculator/out/calculation.txt\n", applicationPath);
    printf("    %s /usr/shared/calc/out/calc.out\n", applicationPath);
}

/* Function to draw the calculator
 * Parameter(s):
 *     calc - Contains the whole calculator state.
 */
void draw(const struct Calc *calc)
{
    system("clear");
    drawCalc(calc);
}

/* Function to read user input and trigger the relevant calculator action.
 * Parameter(s):
 *     calc - Contains the whole calculator state.
 * Returns with true, if the calculation is finished.
 */
bool step(struct Calc *calc)
{
    bool calcOver = false;

    /* Get user input */
    char ch;
    disableBuffer();
    scanf(" %c", &ch);
    enableBuffer();

    /* Convert it to movement or other user action */
    switch (ch)
    {
    case 'w': stepActiveKey(calc, DIR_UP);    break;
    case 'a': stepActiveKey(calc, DIR_LEFT);  break;
    case 's': stepActiveKey(calc, DIR_DOWN);  break;
    case 'd': stepActiveKey(calc, DIR_RIGHT); break;
    case 'e': calcOver = selectActiveKey(calc); break;
    default: /* nothing to do */ break;
    }

    return calcOver;
}

/* The main function what creates, evaluates and destroys the calculator. */
int main(int argc, char* argv[])
{
    const char *applicationPath = argv[0];

    /* Parse command line parameter(s) */
    if (argc != 2)
    {
        /* Invalid count of command line arguments */
        printUsage(applicationPath, "Invalid command line parameters.");
    }
    else
    {
        struct Calc calc;

        /* Initialize random generation */
        srand(time(NULL));

        /* Create the calculator */
        calc = createCalc();

        /* Evaluate calculation */
        draw(&calc);
        while (!step(&calc))
        {
            draw(&calc);
        }

        /* Save the calculation history */
        saveCalcHistory(&calc, argv[1]);

        /* Delete the calculator */
        deleteCalc(&calc);
    }

    return 0;
}
