/**
 * This file contains custom types and functions to handle the calculator state.
 * Calculator state consists of the display state, numpad style, active key and the actual output value.
 */

#ifndef CALC_H
#define CALC_H

#include "numpad.h"
#include "types/linked_list.h"
#include "types/types.h"

/* Type to represent the state of the calculator */
struct Calc {
    int actualValue;
    struct Numpad numpad;
    int sumValue;
    struct LinkedListHead numberHistory;
};

/* Function to create a calculator.
 * Returns with the created calculator descriptor structure.
 * Warning: The calculator must be deinitialized by calling deleteCalc function!
 */
struct Calc createCalc();

/* Function to deinitialize the calculator.
 * Parameter(s):
 *     calculator - the calculator to be deinitialized.
 * Warning: Calculator shall not be used after this call!
 */
void deleteCalc(struct Calc *calculator);

/* Function to step the active pos.
 * Parameter(s):
 *     calculator - the calculator what will be updated
 *     direction - the required direction
 * Returns with true, if the required step is possible.
 */
bool stepActiveKey(struct Calc *calculator, const enum Dir direction);

/* Function to check the goal is reached by the player, snake eats the player or not.
 * Parameter(s):
 *     calculator - The calculator itself with all properties and items.
 * Returns with true, if the calculation is over.
 */
bool selectActiveKey(struct Calc *calculator);

/* Function to draw the calculator
 * Parameter(s):
 *     calculator - The calculator to draw.
 */
void drawCalc(const struct Calc *calculator);

/* Function to save and reset the calculation history
 * Parameter(s):
 *     outFileName - the name of the file to save the calculation history.
 */
void saveCalcHistory(struct Calc *calculator, const char *filepath);

#endif /* CALC_H */
