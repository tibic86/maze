/**
 * This file defines a structure to store the numpad state of the calculator.
 * There is a function to create and delete it and there are anothers to change the actual marker position, 
 * get the actually marked key and draw the numpad.
 */

#ifndef NUMPAD_H
#define NUMPAD_H

#include "types/types.h"

/* Type to store the size, symbols and actually marked position of the numpad */
struct Numpad {
    struct Size size;
    char **symbols;
    struct Pos activePos;
};

/* Function to allocate and initialize the numpad.
 * Parameter(s):
 *     size - the size of the numpad.
 *     symbols - the key symbols as a string from left-to-right and top-to-bottom.
 * Returns with the created numpad descriptor structure.
 * Warning: the allocated numpad have to be deallocated by calling deleteNumpad function.
 */
struct Numpad createNumpad(const struct Size size, const char *symbols);

/* Function to deallocate a previuosly allocated numpad.
 * Parameter(s):
 *     numpad - the numpad descriptor structure
 */
void deleteNumpad(struct Numpad *numpad);

/* Function to change the position of the marker.
 * Parameter(s):
 *     numpad - the numpad to be update
 *     direction - the requested direction to move the marker
 * Returns true, if the step is possible.
 */
bool stepActive(struct Numpad *numpad, const enum Dir direction);

/* Function to get the actually marked key.
 * Parameter(s):
 *     numpad - the numpad descriptor structure
 * Returns with the symbol of the actually marked position.
 */
char getActive(const struct Numpad *numpad);

/* Function to draw the numpad to the standard output.
 * Parameter(s):
 *     numpad- the numpad to draw
 */
void drawNumpad(const struct Numpad *numpad);

#endif /* NUMPAD_H */
