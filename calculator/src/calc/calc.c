/**
 * This file is the implementation of the calc.h.
 * See it for more details.
 */

#include "calc.h"

#include <stdlib.h>
#include <stdio.h>

struct Calc createCalc(const char *outFileName)
{
    struct Calc calc;
    int randomNumber;

    /* Initialize actual value */
    calc.actualValue = 0;

    /* Create numpad - depends on the random number */
    randomNumber = rand(); /* Get a random number between 0 and RAND_MAX */
    if (randomNumber <= RAND_MAX/2)
    {
        const struct Size size = { 4, 3 };
        const char symbols[] = 
            "123"
            "456"
            "789"
            "+0="
        ;
        calc.numpad = createNumpad(size, symbols);
    }
    else
    {
        const struct Size size = { 4, 3 };
        const char symbols[] = 
            "+0="
            "123"
            "456"
            "789"
        ;
        calc.numpad = createNumpad(size, symbols);
    }

    /* Initialize the current total and the history */
    calc.sumValue = 0;
    calc.numberHistory = createLinkedList();

    return calc;
}

void deleteCalc(struct Calc *calculator)
{
    deleteLinkedList(&calculator->numberHistory);
    deleteNumpad(&calculator->numpad);
}

bool stepActiveKey(struct Calc *calculator, const enum Dir direction)
{
    /* Pass this trigger to the number pad */
    return stepActive(&calculator->numpad, direction);
}

bool selectActiveKey(struct Calc *calculator)
{
    bool calculationOver = false;
    const char key = getActive(&calculator->numpad); /* get pressed key */

    if (key >= '0' && key <= '9')
    {
        /* It is a number */
        if (calculator->actualValue < 1000000)
        {
            /* Update actual value */
            const int value = key - '0';
            calculator->actualValue = calculator->actualValue * 10 + value;
        }
    }
    else if (key == '+')
    {
        /* It is the + sign, update current total, history and reset actual value */
        calculator->sumValue += calculator->actualValue;
        pushToLinkedList(&calculator->numberHistory, &calculator->actualValue, sizeof(calculator->actualValue));
        calculator->actualValue = 0;
    }
    else if (key == '=')
    {
        /* It is the = sign, request exit */
        calculationOver = true;
    }
    else
    {
        /* Invalid key selected (should never go here), ignore */
    }

    return calculationOver;
}

void drawCalc(const struct Calc *calculator)
{
    /* Print actual value with the border */
    printf("---------\n");
    printf("|%7d|\n", calculator->actualValue);
    printf("---------\n");

    /* Print numpad */
    drawNumpad(&calculator->numpad);

    /* Print the current sum */
    printf("Current total: %d\n", calculator->sumValue);
}

void saveCalcHistory(struct Calc *calculator, const char *filepath)
{
    /* Open the file for write in text mode */
    FILE *f = fopen(filepath, "wt");
    if (f)
    {
        /* Get first item */
        int *value = (int*)popFromLinkedList(&calculator->numberHistory);
        if (!value)
        {
            /* List is empty */
            fprintf(f, "0");
        }
        else
        {
            /* List has item(s), print the first one */
            fprintf(f, "%d", *value);

            /* Get the second item */
            value = (int*)popFromLinkedList(&calculator->numberHistory);
            if (value)
            {
                /* List has other item(s), print the second and following one(s) */
                do
                {
                    fprintf(f, " + %d", *value);
                    value = (int*)popFromLinkedList(&calculator->numberHistory);
                }
                while (value);
                /* Print the result */
                fprintf(f, " = %d", calculator->sumValue);
            }
        }
        fclose(f);
    }
}
