/**
 * This file is the implementation of the numpad.h.
 * See it for more details.
 */

#include "numpad.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* -------------------------------------------------------------------------- */
/* PRIVATE DECLARATION(S) */
/* -------------------------------------------------------------------------- */

/* Private function to draw the symbols of a line of the numpad.
 * Parameter(s):
 *     numpad - the numpad descriptor structure
 *     line - the line to draw
 */
void _drawNumpadLineSymbols(const struct Numpad *numpad, const int line);

/* Private function to draw the marker below a numpad line.
 * Parameter(s):
 *     numpad - the numpad descriptor structure
 *     line - the line number before the space of the marker
 * Note: this function will draw empty spaces if not the position have to be marked,
 *     otherwise it will draw the marker symbol.
 */
void _drawNumpadLineMarker(const struct Numpad *numpad, const int line);


/* -------------------------------------------------------------------------- */
/* PUBLIC DEFINITION(S) */
/* -------------------------------------------------------------------------- */

struct Numpad createNumpad(const struct Size size, const char *symbols)
{
    const int symbolLen = strlen(symbols);
    int p = 0;
    struct Numpad numpad;

    /* Allocate and initialize a 2D array for the symbols */
    char **numpadSymbols = (char**)malloc(size.height * sizeof(char*));
    int y;
    for(y = 0; y < size.height; ++y)
    {
        int x;
        numpadSymbols[y] = (char*)malloc(size.width * sizeof(char));
        for(x = 0; x < size.width; ++x)
        {
            /* Initialize the symbol of the actual key */
            if (p < symbolLen)
            {
                /* Copy the symbol from the parameter */
                numpadSymbols[y][x] = symbols[p++];
            }
            else
            {
                /* This symbol is not defined, it will be empty */
                numpadSymbols[y][x] = ' ';
            }
        }
    }

    /* Initialize the structure */
    numpad.activePos.x = 0;
    numpad.activePos.y = 0;
    numpad.symbols = numpadSymbols;
    numpad.size = size;

    return numpad;
}

void deleteNumpad(struct Numpad *numpad)
{
    int y;
    const int height = numpad->size.height;

    /* Reset size */
    numpad->size.width = 0;
    numpad->size.height = 0;

    /* Deallocate the 2D array */
    for(y = 0; y < height; ++y)
    {
        free(numpad->symbols[y]);
    }
    free(numpad->symbols);
}

bool stepActive(struct Numpad *numpad, const enum Dir direction)
{
    bool succeed = false;

    /* Calculate new position */
    struct Pos newPos = numpad->activePos;
    switch (direction)
    {
    case DIR_UP:    newPos.y--; break;
    case DIR_RIGHT: newPos.x++; break;
    case DIR_DOWN:  newPos.y++; break;
    case DIR_LEFT:  newPos.x--; break;
    }

    /* Check the new position is inside the numpad area or not */
    if (newPos.x >= 0 && newPos.x < numpad->size.width &&
        newPos.y >= 0 && newPos.y < numpad->size.height)
    {
        /* New position is inside the numpad area, update active position */
        numpad->activePos = newPos;
        succeed = true;
    }

    return succeed;
}

char getActive(const struct Numpad *numpad)
{
    return numpad->symbols[numpad->activePos.y][numpad->activePos.x];
}

void drawNumpad(const struct Numpad *numpad)
{
    int y;
    for(y = 0; y < numpad->size.height; ++y)
    {
        _drawNumpadLineSymbols(numpad, y);
        _drawNumpadLineMarker(numpad, y);
    }
}


/* -------------------------------------------------------------------------- */
/* PRIVATE DEFINITION(S) */
/* -------------------------------------------------------------------------- */

void _drawNumpadLineSymbols(const struct Numpad *numpad, const int line)
{
    int x;
    for(x = 0; x < numpad->size.width; ++x)
    {
        printf("%c ", numpad->symbols[line][x]);
    }
    printf("\b\n");
}

void _drawNumpadLineMarker(const struct Numpad *numpad, const int line)
{
    int x;
    for(x = 0 ; x < numpad->size.width; ++x)
    {
        if (line == numpad->activePos.y && x == numpad->activePos.x)
        {
            printf("^ ");
        }
        else
        {
            printf("  ");
        }
    }
    printf("\b\n");
}
