/**
 * This file defines a structure to store the head of a linked list what contains generic (void*) data in each item.
 * There is a function to allocate and initialize it and there is anothers to append, remove item or get the length of the list.
 */

#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include "types.h"

/* Type to represent an item in the linked list */
struct LinkedListItem;

/* Type to represent the head of the linked list */
struct LinkedListHead
{
    struct LinkedListItem *last;
};

/* Function to create an empty linked list.
 * Returns with the head of the linked list.
 * Warning: The linked list must be destroyed by calling deleteLinkedList when it is not required anymore!
 */
struct LinkedListHead createLinkedList();

/* Function to destroy a linked list.
 * Parameter(s):
 *     list - The head of the linked list to be deleted.
 */
void deleteLinkedList(struct LinkedListHead *list);

/* Function to append an item to the end of a linked list.
 * Parameter(s):
 *     list - The head of the list to be changed.
 *     data (optional) - The data of the new item.
 *     size - The size of the new item.
 * Returns true, if the insertion succeed.
 */
bool pushToLinkedList(struct LinkedListHead *list, const void *data, const unsigned size);

/* Function to remove an item from the end of a linked list.
 * Parameter(s):
 *     list - The head of the list to be changed.
 * Returns with the data of the last item if there is any, otherwise with NULL.
 */
void* popFromLinkedList(struct LinkedListHead *list);

/* Function to get the data of the last item in a linked list.
 * Parameter(s):
 *     list - The head of the list.
 * Returns with the number of the items in the linked list.
 */
int getLinkedListLength(const struct LinkedListHead list);

#endif /* LINKED_LIST_H */
