/**
 * This file defines all of the used standard and custom types.
 */

#ifndef TYPES_H
#define TYPES_H

#include <stdbool.h>

#ifndef NULL
#define NULL ((void *)0)
#endif

/* Type to represent 2D size */
struct Size
{
    int height;
    int width;
};

/* Type to represent 2D position */
struct Pos {
    int y;
    int x;
};

/* Type to represent direction */
enum Dir {
    DIR_UP,
    DIR_LEFT,
    DIR_DOWN,
    DIR_RIGHT
};

#endif /* TYPES_H */
